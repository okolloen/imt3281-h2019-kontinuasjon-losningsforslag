package no.ntnu.imt3281.eksamen;

import java.sql.*;

import static spark.Spark.*;

/**
 * Kode som brukes om utgangspunkt for oppgave 2, swapi server
 */
public class App {

    public static void main(String[] args) {
        String dbURL = "jdbc:derby:../swapiDB";
        Connection con = null;
        try {                                                   // Attempt to connect to DB
            con = DriverManager.getConnection(dbURL);
        } catch (SQLException e) {                              // if it failed
            System.err.println("Kunne ikke koble til databasen");
            System.exit(1);
        }
        PreparedStatement stmt=null;
        try {
            stmt = con.prepareStatement("SELECT json FROM swapi WHERE type=? AND nr=?");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        final PreparedStatement stmnt = stmt;   // Must be defined final in lambda function

        get("/:type/:nr", (req, res) -> {
            res.type("application/json");

            stmnt.setString(1, req.params("type"));
            stmnt.setInt(2, Integer.parseInt(req.params("nr")));
            ResultSet rs = stmnt.executeQuery();    // Get correct entry from DB
            if (rs.next()) {
                return rs.getString(1); // Found in DB, return the record
            } else {
                res.status(401);    // 401 is not found
                return "{\"status\": \"ERROR\", \"reason\": \"No such resource\"}";
            }
        });
    }
}
