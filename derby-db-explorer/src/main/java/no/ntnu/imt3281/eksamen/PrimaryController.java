package no.ntnu.imt3281.eksamen;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.sql.*;
import java.util.Optional;

public class PrimaryController {
    Connection con = null;
    ResultSet data = null;
    String selectSQL = null;
    ContextMenu tableContextMenu = new ContextMenu();
    ObservableList<String[]> tableData = FXCollections.observableArrayList();

    @FXML
    private Label derby_db;

    @FXML
    private ListView<String> tableSelect;

    @FXML
    private TextArea sql;

    @FXML
    private TableView<String[]> table;
    private Stage stage;

    /**
     * Runs the SQL Query in the textarea "sql" on the active connection.
     *
     * @param event unused, implementation side effect.
     */
    @FXML
    void execute(ActionEvent event) {
        try {
            if (con!=null) {
                con.createStatement().execute(sql.getText());
                sqlSelect();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show the context meny for the table containing a view of the database.
     *
     * @param event used to get the location to show the pop menu
     */
    @FXML
    void tableContextMenu(ContextMenuEvent event) {
        tableContextMenu.show(table, event.getScreenX(), event.getScreenY());
    }

    /**
     * Execute the query in the textarea "sql", show the result in the table.
     *
     * @param event unused, implmentation side effect
     */
    @FXML
    void executeQuery(ActionEvent event) {
        selectSQL = sql.getText();  // Store the query for further reference
        sqlSelect();                // Actually run the query
    }

    /**
     * Let user select the Derby database to work on. Will connect to the database and get a list of
     * user tables, these are shown in the listView "tableSelect" to let user easily show the contents
     * of different tables in the database.
     *
     * @param event unused, implementation side effect
     */
    @FXML
    void selectDerbyDB(ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory!=null) {  // User made a selection
            try {
                if (data!=null) {       // Already connected, close
                    data.close();
                }
                if (con!=null) {
                    con.close();
                }
                con = DriverManager.getConnection("jdbc:derby:"+selectedDirectory.getAbsolutePath());
                derby_db.setText(selectedDirectory.getAbsolutePath());
                String SQL = "SELECT TABLENAME FROM SYS.SYSTABLES WHERE TABLETYPE='T' ORDER BY TABLENAME";
                ResultSet res = con.createStatement().executeQuery(SQL);
                ObservableList<String> tables = FXCollections.observableArrayList();
                while (res.next())      // Fill in table names in list of tables
                    tables.add(res.getString(1));
                tableSelect.setItems(tables);
            } catch (SQLException e) {  // In case of error, show instead of name of selected database
                derby_db.setText(e.getMessage());
            }
        }
    }

    /**
     * This method runs when all components from the FXML file has been initialized.
     * Used to set up context menu, adds selection listener to the list of table names.
     *
     */
    @FXML
    void initialize() {
        // Add listener on list of tables in database
        tableSelect.getSelectionModel().selectedItemProperty().addListener(((observableValue, oldValue, newValue) -> {
            selectSQL = "SELECT * FROM "+newValue;
            sqlSelect();
        }));

        table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);   // Only allow single row selection

        MenuItem delete = new MenuItem("Delete");   // Popup menu delete item
        delete.setOnAction(e->{
            try {
                // Must use orginal idx (from DB) since table indexes will change when sorted
                int originalIdx = Integer.parseInt(table.getSelectionModel().getSelectedItem()[table.getSelectionModel().getSelectedItem().length-1]);
                data.absolute(originalIdx+1);     // +1 since SQL always counts from 1
                data.deleteRow();
                tableData.remove(table.getSelectionModel().getSelectedIndex());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        MenuItem edit = new MenuItem("Edit");   // Popup menu edit item
        edit.setOnAction(e->{
            String[] selected = table.getSelectionModel().getSelectedItem();
            showEditDialog(selected);               // Edit selected item
        });
        tableContextMenu.getItems().addAll(delete, edit);   // Add menu items to popup menu
    }

    /**
     * Brings up dialog window that let the user edit all information about this item.
     * Stores it back to the database and updates the table on screen.
     *
     * @param selected String[] containing information about the item to be edited.
     */
    private void showEditDialog(String[] selected) {
        Dialog<String[]> dialog = new Dialog<>();   // Create a Dialog to edit an array of Strings
        dialog.setTitle("Endre post");
        dialog.setHeaderText("Gjør dine endringer i dataene her.");
        dialog.setResizable(true);

        GridPane grid = new GridPane();

        ResultSetMetaData meta = null;
        int columns = 0;
        try {
            meta = data.getMetaData();
            columns = meta.getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final TextField editFields[] = new TextField[columns];
        for (int i=0; i<columns; i++) {     // Add labels with title from DB table and text field to edit content
            Label l = null;
            try {
                l = new Label(meta.getColumnName(i+1));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            editFields[i] = new TextField(selected[i]);
            grid.add(l, 1, i+1);
            grid.add(editFields[i], 2, i+1);
        }

        dialog.getDialogPane().setContent(grid);

        ButtonType buttonTypeOk = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
        dialog.getDialogPane().getButtonTypes().add(buttonTypeCancel);

        dialog.setResultConverter(new Callback<ButtonType, String[]>() {    // Used to return the result when OK button is pressed in dialog
            @Override
            public String[] call(ButtonType b) {

                if (b == buttonTypeOk) {
                    String[] tmp = new String[editFields.length];
                    for (int i=0; i<tmp.length; i++)
                        tmp[i] = editFields[i].getText();
                    return tmp;
                }

                return null;
            }
        });

        Optional<String[]> result = dialog.showAndWait();

        if (result.isPresent()) {   // Update database and user view
            try {
                int originalIdx = Integer.parseInt(table.getSelectionModel().getSelectedItem()[table.getSelectionModel().getSelectedItem().length-1]);
                data.absolute(originalIdx+1);
                String[] tmp = new String[result.get().length+1];
                for (int i=0; i<result.get().length; i++) {
                    data.updateString(i+1, result.get()[i]);
                    tmp[i] = result.get()[i];
                }
                data.updateRow();
                tmp[tmp.length-1] = Integer.toString(originalIdx);  // Store original index with edited data
                tableData.set(table.getSelectionModel().getSelectedIndex(), tmp);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Run an SQL query and fill the table in the UI with the result.
     */
    private void sqlSelect() {
        sql.setText(selectSQL);
        try {
            data = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE).executeQuery(selectSQL);    // Must use this kind of resultSet to be able to edit all data.
            ResultSetMetaData meta = data.getMetaData();
            int columns = meta.getColumnCount();
            table.getItems().clear();
            table.getColumns().clear();
            for (int i=0; i<columns; i++) {     // Found on https://www.codesd.com/item/fill-tableview-with-a-two-dimensional-array.html
                TableColumn tc = new TableColumn(meta.getColumnName(i+1));
                tc.setMaxWidth(600);

                tc.setPrefWidth(600/columns);
                final int colNo = i;
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        return new SimpleStringProperty((p.getValue()[colNo]));
                    }
                });
                table.getColumns().add(tc);
            }

            tableData.clear();
            int idx = 0;
            while (data.next()) {
                String[] tmp = new String[columns+1];
                for(int i=0; i<columns; i++) {
                    tmp[i] = data.getString(i+1);
                }
                tableData.add(tmp);
                tmp[columns] = Integer.toString(idx);   // Store original idx in last element, will not be shown.
                idx++;
            }
            table.setItems(tableData);           // End of found on https://www.codesd.com/item/fill-tableview-with-a-two-dimensional-array.html
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Enable app to transfer the stage to the controller, used to show dialog windows
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    // Enable app to close resultset and connection, must do this to actually store changes to DB.
    public void cleanUp() {
        try {
            if (data!=null)
               data.close();
            if (con!=null)
                con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
