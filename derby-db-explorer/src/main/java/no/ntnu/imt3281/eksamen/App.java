package no.ntnu.imt3281.eksamen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static PrimaryController controller;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary", stage));
        stage.setScene(scene);
        stage.show();
    }

    private static Parent loadFXML(String fxml, Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        Parent scene = fxmlLoader.load();
        controller = fxmlLoader.getController();
        controller.setStage(stage);
        return scene;
    }

    @Override
    public void stop(){
        System.out.println("Stage is closing");
        controller.cleanUp();
    }

    public static void main(String[] args) {
        launch();
    }

}